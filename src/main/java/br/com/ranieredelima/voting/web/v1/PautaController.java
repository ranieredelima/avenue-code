package br.com.ranieredelima.voting.web.v1;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ranieredelima.voting.service.PautaService;
import br.com.ranieredelima.voting.web.v1.request.BodyRequest;
import io.swagger.annotations.Api;

@Api(value = "Responsável gerar as views da aplicação mobile", tags = {"Pauta"})
@RestController
@RequestMapping(path = PautaController.API_V1_PAUTA)
@Validated
public class PautaController {
	
	public static final String API_V1_PAUTA = "/v1/pauta";
	private PautaService pautaService;

	public PautaController(PautaService pautaService) {
		this.pautaService = pautaService;
	}
	
	@PostMapping
	public ResponseEntity<Object> cadastrarPauta(@RequestBody @Valid final BodyRequest request){
		return ResponseEntity.ok(pautaService.cadastrar(request));
	}
	
	@PostMapping("/iniciar-votacao")
	public ResponseEntity<String> iniciarVotacao(@RequestBody @Valid final BodyRequest request){
		pautaService.inicarVotacao(request);
		return ResponseEntity.ok("OK");
	}
	
	@PostMapping("/votar")
	public ResponseEntity<String> realizarVotacao(@RequestBody @Valid final BodyRequest request){
		pautaService.votar(request);
		return ResponseEntity.ok("VOTO CADASTRADO");
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> realizarVotacao(@PathVariable Long id){
		return ResponseEntity.ok(pautaService.obterDados(id));
	}

}
