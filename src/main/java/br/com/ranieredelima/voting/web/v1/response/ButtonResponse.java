package br.com.ranieredelima.voting.web.v1.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.ranieredelima.voting.web.v1.request.BodyRequest;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Objeto retornado para montar as opções da tela principal do cliente")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ButtonResponse extends ItemResponse  implements Serializable {

	private static final long serialVersionUID = -3836415440705591832L;

	private BodyRequest body;
	
}
