package br.com.ranieredelima.voting.web.v1.assembler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ranieredelima.voting.model.api.Pauta;
import br.com.ranieredelima.voting.model.api.PautaStatus;
import br.com.ranieredelima.voting.web.v1.ViewController;
import br.com.ranieredelima.voting.web.v1.response.ButtonResponse;

@Component
public class ButtonResponseAssembler {

	@Value("${vottingapi.domain}")
	private String urlDomain;

	private BodyAssembler bodyAssembler;
	
	public ButtonResponseAssembler(
		BodyAssembler bodyAssembler	
	) {
		this.bodyAssembler = bodyAssembler;
	}

	public ButtonResponse assemblerCancelar() {
		return ButtonResponse.builder()
				.texto("Cancelar")
				.url(this.urlDomain + ViewController.VIEWS_V1_PAUTAS)
			.build();
	}

	public ButtonResponse assemblerDefaultOk() {
		return ButtonResponse.builder()
				.texto("Cadastrar")
				.url(this.urlDomain + "v1/pauta")
				.body(this.bodyAssembler.assemblerDefault())
			.build();
	}

	public ButtonResponse assemblerOk(Pauta pauta) {
		return PautaStatus.EM_VOTACAO.equals(pauta.getStatus()) 
				? this.assemblerVotar(pauta) 
				: this.assemblerIniciarVotacao(pauta);
	}

	private ButtonResponse assemblerVotar(Pauta pauta) {
		return ButtonResponse.builder()
				.texto("Votar")
				.url(this.urlDomain + "v1/pauta/votar")
				.body(this.bodyAssembler.assembler(pauta))
			.build();
	}

	private ButtonResponse assemblerIniciarVotacao(Pauta pauta) {
		return ButtonResponse.builder()
				.texto("Iniciar Votacao")
				.url(this.urlDomain + "v1/pauta/iniciar-votacao")
				.body(this.bodyAssembler.assembler(pauta))
			.build();
	}

}
