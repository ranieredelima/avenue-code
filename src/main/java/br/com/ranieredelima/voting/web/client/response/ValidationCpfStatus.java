package br.com.ranieredelima.voting.web.client.response;

public enum ValidationCpfStatus {

	ABLE_TO_VOTE,
	UNABLE_TO_VOTE,
}
