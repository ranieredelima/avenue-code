package br.com.ranieredelima.voting.web.v1;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ranieredelima.voting.service.ViewService;
import br.com.ranieredelima.voting.web.v1.response.ViewResponse;
import io.swagger.annotations.Api;

@Api(value = "Responsável gerar as views da aplicação mobile", tags = {"View"})
@RestController
@RequestMapping(path = ViewController.VIEWS_V1_PAUTAS)
@Validated
public class ViewController {
	
	public static final String VIEWS_V1_PAUTAS = "/v1/views/pautas";
	private final ViewService viewService;

	public ViewController(ViewService viewService) {
		this.viewService = viewService;
	}
	
	@GetMapping()
	public ResponseEntity<ViewResponse> listarPautas(Pageable pageable){
		return ResponseEntity.ok(viewService.listarVotacoes(pageable));
	}
	
	@GetMapping("/new")
	public ResponseEntity<ViewResponse> iniciarVotacao(){
		return ResponseEntity.ok(viewService.cadastrarVotacao());
	}
	
	@GetMapping("/{idPauta}")
	public ResponseEntity<ViewResponse> obterPauta(@PathVariable("idPauta") Long idPauta ){
		return ResponseEntity.ok(viewService.realizarVotacao(idPauta));
	}
	

}
