package br.com.ranieredelima.voting.web.v1.request;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Request para realizar o cadastro de uma pauta")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VotoRequest {
	
	private String voto;

}
