package br.com.ranieredelima.voting.web.v1.request;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Objeto recebido/retornado na requisição do cliente")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BodyRequest implements Serializable {
	
	private static final long serialVersionUID = 4031007454948478811L;

	private String dadosOpcao;

	@JsonProperty("campo1")
	@NotBlank
	private String cpf;
	
	@JsonProperty("campo2")
	@NotNull
	private Long identificador;

//	@JsonAnyGetter
	@JsonAnySetter
	private Map<String, Object> campos = new HashMap<>();
	
}
