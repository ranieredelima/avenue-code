package br.com.ranieredelima.voting.web.v1.response;

public enum ItemType {

	TEXTO,
	INPUT_TEXTO,
	INPUT_NUMERO,
	INPUT_DATA,
	INPUT_OPTION,
	OPTION
}
