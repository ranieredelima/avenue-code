package br.com.ranieredelima.voting.web.v1.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Objeto retornado para montar a tela principal de visualizacao do cliente")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ViewResponse implements Serializable {

	private static final long serialVersionUID = -1575817774156033706L;
	private ViewType tipo;
	private String titulo;
	private List<ItemResponse> itens;
	private ButtonResponse botaoOk;
	private ButtonResponse botaoCancelar;
	
}
