package br.com.ranieredelima.voting.web.v1.assembler;

import org.springframework.stereotype.Component;

import br.com.ranieredelima.voting.model.api.Pauta;
import br.com.ranieredelima.voting.util.GeradorCpf;
import br.com.ranieredelima.voting.web.v1.request.BodyRequest;

@Component
public class BodyAssembler {

	public BodyRequest assemblerDefault() {
		return BodyRequest.builder()
				.cpf( GeradorCpf.cpf())
				.build();
	}

	public BodyRequest assembler(Pauta pauta) {
		return BodyRequest.builder()
				.cpf( GeradorCpf.cpf() )
				.identificador(pauta.getId())
				.build();
	}

}
