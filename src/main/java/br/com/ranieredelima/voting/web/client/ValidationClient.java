package br.com.ranieredelima.voting.web.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.ranieredelima.voting.web.client.response.ValidationCpfResponse;

@FeignClient(name = "ValidationCpfAbleVoting", url = "${vottingapi.validationcpf.url}")
public interface ValidationClient {
	
	@GetMapping("/users/{cpf}")
	ValidationCpfResponse validateCpf(
		@PathVariable("cpf") String cpf
	);

}
