package br.com.ranieredelima.voting.web.v1.assembler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ranieredelima.voting.model.api.Pauta;
import br.com.ranieredelima.voting.model.api.PautaStatus;
import br.com.ranieredelima.voting.model.api.TipoVoto;
import br.com.ranieredelima.voting.util.GeradorCpf;
import br.com.ranieredelima.voting.web.v1.request.BodyRequest;
import br.com.ranieredelima.voting.web.v1.response.ButtonResponse;
import br.com.ranieredelima.voting.web.v1.response.ItemResponse;
import br.com.ranieredelima.voting.web.v1.response.ItemType;

@Component
public class ItemViewAssembler {

	@Value("${vottingapi.domain}")
	private String urlDomain;
	
	public List<ItemResponse> assembler(List<Pauta> content) {
		return content == null ? null :
			content.stream()
				.map(pauta -> assembler(pauta))
				.collect(Collectors.toList());
	}

	public ItemResponse assembler(Pauta pauta) {
		return ItemResponse.builder()
				.texto(pauta.getTitulo())
				.url(this.urlDomain + "v1/views/pautas/" + pauta.getId())
				.build();
	}

	public List<ItemResponse> assemblerDefault() {
		List<ItemResponse> itens = new ArrayList<>();
		
		// HEADER
		itens.add(
			ItemResponse.builder()
			.tipo(ItemType.TEXTO)
			.texto("Tela para realizar o cadastro de uma nova pauta")
			.build()
		);
		
		// INPUTS FORM
		itens.add(
			ItemResponse.builder()
			.tipo(ItemType.INPUT_TEXTO)
			.id("titulo")
			.titulo("Nova Pauta")
			.build()
		);
		itens.add(
			ItemResponse.builder()
			.tipo(ItemType.INPUT_NUMERO)
			.id("minutos")
			.titulo("Duração para votação (Em minutos)")
			.build()
		);
		itens.add(
			ItemResponse.builder()
			.tipo(ItemType.INPUT_DATA)
			.id("dataVotacao")
			.titulo("Data da Votação")
			.build()
		);
		
		return itens;
	}

	public List<ItemResponse> assemblerVotacao(Pauta pauta) {
		List<ItemResponse> itens = new ArrayList<>();
		
		if(!PautaStatus.EM_VOTACAO.equals(pauta.getStatus())) 
			return itens;
		
		// HEADER
		itens.add(
			ItemResponse.builder()
				.tipo(ItemType.TEXTO)
				.texto("Realize a votação da pauta abaixo")
			.build()
		);
		
		// INPUTS FORM
		itens.add(
			ButtonResponse.builder()
				.tipo(ItemType.INPUT_OPTION)
				.titulo("Sim")
				.body(
					BodyRequest.builder()
						.cpf(GeradorCpf.cpf())
						.identificador(pauta.getId())
						.dadosOpcao(TipoVoto.SIM.name())
					.build()
				)
			.build()
		);
		
		itens.add(
			ButtonResponse.builder()
				.tipo(ItemType.INPUT_OPTION)
				.titulo("Não")
				.body(
					BodyRequest.builder()
						.cpf(GeradorCpf.cpf())
						.identificador(pauta.getId())
						.dadosOpcao(TipoVoto.NAO.name())
					.build()
				)
			.build()
		);
		
		return itens;
	}

}
