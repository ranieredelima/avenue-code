package br.com.ranieredelima.voting.web.v1.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Objeto retornado para montar as opções da tela principal do cliente")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItemResponse implements Serializable {
	
	private static final long serialVersionUID = 6805693205505547889L;

	private ItemType tipo;

	private String id;
	private String texto;
	private String titulo;
	private String url;
	private Object valor;

}
