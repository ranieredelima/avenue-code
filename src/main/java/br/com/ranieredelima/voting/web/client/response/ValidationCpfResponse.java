package br.com.ranieredelima.voting.web.client.response;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ValidationCpfResponse implements Serializable{

	private static final long serialVersionUID = 7933226756378587138L;
	private ValidationCpfStatus status;

}
