package br.com.ranieredelima.voting.web.v1.assembler;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.ranieredelima.voting.model.api.Pauta;
import br.com.ranieredelima.voting.web.v1.response.ViewResponse;
import br.com.ranieredelima.voting.web.v1.response.ViewType;

@Component
public class ViewAssembler {

	private ItemViewAssembler itemViewAssembler;
	private ButtonResponseAssembler buttonAssembler;

	public ViewAssembler(
		ItemViewAssembler itemViewAssembler,
		ButtonResponseAssembler buttonAssembler
	) {
		this.itemViewAssembler = itemViewAssembler;
		this.buttonAssembler = buttonAssembler;
	}

	public ViewResponse assembler(List<Pauta> content) {
		return ViewResponse.builder()
				.titulo("LISTAGEM DE PAUTAS")
				.tipo(ViewType.SELECAO)
				.itens(this.itemViewAssembler.assembler(content))
				.build();
	}

	public ViewResponse assembler(Pauta pauta) {
		return ViewResponse.builder()
				.titulo("VOTAÇÃO - " + pauta.getTitulo())
				.tipo(ViewType.FORMULARIO)
				.itens(this.itemViewAssembler.assemblerVotacao(pauta) )
				.botaoOk(this.buttonAssembler.assemblerOk(pauta))
				.botaoCancelar( this.buttonAssembler.assemblerCancelar() )
				.build();
	}

	public ViewResponse assemblerDefault() {
		return ViewResponse.builder()
				.titulo("CADASTRO DE PAUTA")
				.tipo(ViewType.FORMULARIO)
				.itens(this.itemViewAssembler.assemblerDefault())
				.botaoOk(
					this.buttonAssembler.assemblerDefaultOk()
				)
				.botaoCancelar(
					this.buttonAssembler.assemblerCancelar()
				)
				.build();
	}

}
