package br.com.ranieredelima.voting.web.client;

import org.springframework.stereotype.Component;

import br.com.ranieredelima.voting.exception.InvalidRequestException;
import br.com.ranieredelima.voting.util.GeradorCpf;
import br.com.ranieredelima.voting.web.client.response.ValidationCpfResponse;
import br.com.ranieredelima.voting.web.client.response.ValidationCpfStatus;

@Component
public class ValidationClientComponent {

	private ValidationClient client;

	public ValidationClientComponent(ValidationClient client) {
		this.client = client;
	}
	
	public boolean validateClient(String cpf) {
		if(cpf == null || cpf.length() == 0) {
			throw new InvalidRequestException("CPF não informado");
		} else if(!GeradorCpf.isCPF(cpf)) {
			throw new InvalidRequestException("CPF informado não é valido");
		}
		
		ValidationCpfResponse validateClient = this.client.validateCpf(cpf);
		System.out.println(validateClient);
		if(!ValidationCpfStatus.ABLE_TO_VOTE.equals(validateClient.getStatus())) {
			throw new InvalidRequestException("CPF informado não está permitido para a votação");
		}
		
		return true;
	}
}
