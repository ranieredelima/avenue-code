package br.com.ranieredelima.voting.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.ranieredelima.voting.util.MensagemConstantes;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = MensagemConstantes.ERRO_INTERNO)
public class GeneralErrorException extends RuntimeException{

	private static final long serialVersionUID = 6335920236090454694L;

	public GeneralErrorException(String exception) {
		super(exception);
	}

	public GeneralErrorException(Exception exception) {
		super(exception);
	}

	public GeneralErrorException(String exception, Exception e) {
		super(exception, e);
	}
}
