package br.com.ranieredelima.voting.exception.handler;

import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.ranieredelima.voting.exception.AuthErrorException;
import br.com.ranieredelima.voting.exception.GeneralErrorException;
import br.com.ranieredelima.voting.exception.InvalidRequestException;
import br.com.ranieredelima.voting.exception.NotFoundException;


@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> contraintViolationHandler(ConstraintViolationException ex,
                                                            WebRequest request) {
        String message = ex.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining(","));
      
        CustomErrorAttributes errorAttributes = new CustomErrorAttributes(HttpStatus.BAD_REQUEST, request, message);
        return handleException(ex, errorAttributes, request);
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<Object> methodArgumentTypeMismatchHandler(MethodArgumentTypeMismatchException ex,
                                                                    WebRequest request) {
        CustomErrorAttributes errorAttributes = new CustomErrorAttributes(HttpStatus.BAD_REQUEST, request, ex.getMessage());
        return handleException(ex, errorAttributes, request);
    }

    @ExceptionHandler({InvalidRequestException.class})
    public ResponseEntity<Object> handlerInvalidRequestException(InvalidRequestException ex, WebRequest request) {
        CustomErrorAttributes errorAttributes = new CustomErrorAttributes(HttpStatus.BAD_REQUEST, request, ex.getMessage());
        return handleException(ex, errorAttributes, request);
    }

    @ExceptionHandler({AuthErrorException.class})
    public ResponseEntity<Object> handlerAuthException(AuthErrorException ex, WebRequest request) {
        CustomErrorAttributes errorAttributes = new CustomErrorAttributes(HttpStatus.UNAUTHORIZED, request, ex.getMessage());
        return handleException(ex, errorAttributes, request);
    }

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handlerAuthException(NotFoundException ex, WebRequest request) {
        CustomErrorAttributes errorAttributes = new CustomErrorAttributes(HttpStatus.NOT_FOUND, request, ex.getMessage());
        return handleException(ex, errorAttributes, request);
    }

    @ExceptionHandler({GeneralErrorException.class})
    public ResponseEntity<Object> handlerGeneralInternalException(GeneralErrorException ex, WebRequest request) {
        CustomErrorAttributes errorAttributes = new CustomErrorAttributes(HttpStatus.INTERNAL_SERVER_ERROR, request, ex.getMessage());
        return handleException(ex, errorAttributes, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	String message = ex.getBindingResult()
	    	.getAllErrors().stream()
	        .map(b -> b.getDefaultMessage())
	        .collect(Collectors.joining(","));
    	
    	
        CustomErrorAttributes errorAttributes = new CustomErrorAttributes(HttpStatus.BAD_REQUEST, request, message);
        return handleException(ex, errorAttributes, headers, request);
    }
    
    private ResponseEntity<Object> handleException(Exception ex, CustomErrorAttributes errorAttributes, WebRequest request) {
        return handleException(ex, errorAttributes, new HttpHeaders(), request);
    }

    private ResponseEntity<Object> handleException(Exception ex, CustomErrorAttributes errorAttributes, HttpHeaders headers, WebRequest request) {
        return super.handleExceptionInternal(ex, errorAttributes.body(), headers, errorAttributes.getStatus(), request);
    }
}
