package br.com.ranieredelima.voting.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.ranieredelima.voting.util.MensagemConstantes;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = MensagemConstantes.USUARIO_SENHA_INVALIDO)
public class AuthErrorException extends RuntimeException{

	private static final long serialVersionUID = -5409545075577418221L;

	public AuthErrorException(String exception) {
		super(exception);
	}

	public AuthErrorException(Exception exception) {
		super(exception);
	}

	public AuthErrorException(String exception, Exception e) {
		super(exception, e);
	}
	
}
