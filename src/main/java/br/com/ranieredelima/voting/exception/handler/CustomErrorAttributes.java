package br.com.ranieredelima.voting.exception.handler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import lombok.Getter;

@Getter
public class CustomErrorAttributes {


    private HttpStatus status;
    private String message;
    private String path;

    public CustomErrorAttributes(HttpStatus status, WebRequest request, String message) {
        this.status = status;
        this.message = message;
        this.path = ((ServletWebRequest) request).getRequest().getRequestURI();
    }
    
    public CustomErrorAttributes(HttpStatus httpStatus, List<String> messages, String path) {
        this.status = httpStatus;
        this.message = messages != null ? messages.stream().collect( Collectors.joining( ", " ) ) : null;
        this.path = path;
    }

    public Map<String, Object> body() {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("status", this.status.value());
        body.put("error", this.status.getReasonPhrase());
        body.put("message", this.message);
        body.put("path", this.path);
        return body;
    }

}
