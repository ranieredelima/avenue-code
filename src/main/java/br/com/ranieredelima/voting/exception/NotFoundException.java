package br.com.ranieredelima.voting.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.ranieredelima.voting.util.MensagemConstantes;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = MensagemConstantes.NAO_ENCONTRADO_ERROR)
public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NotFoundException(String exception) {
		super(exception);
	}

	public NotFoundException(Exception exception) {
		super(exception);
	}

	public NotFoundException(String exception, Exception e) {
		super(exception, e);
	}

}
