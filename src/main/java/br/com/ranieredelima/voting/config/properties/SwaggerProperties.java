package br.com.ranieredelima.voting.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class SwaggerProperties {

    @Value("${swagger.titulo}")
    private String titulo;
    @Value("${swagger.descricao}")
    private String descricao;
    @Value("${swagger.versao}")
    private String versao;
}
