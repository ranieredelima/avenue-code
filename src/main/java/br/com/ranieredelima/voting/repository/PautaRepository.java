package br.com.ranieredelima.voting.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.ranieredelima.voting.model.api.Pauta;
import br.com.ranieredelima.voting.model.api.PautaStatus;

@Repository
public interface PautaRepository extends PagingAndSortingRepository<Pauta, Long>{

	Page<Pauta> findAllByStatus(PautaStatus status, Pageable pageable);

}
