package br.com.ranieredelima.voting.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.ranieredelima.voting.model.api.Voto;

@Repository
public interface VotoRepository extends PagingAndSortingRepository<Voto, Long>{

	boolean existsByCpfAndIdPauta(String cpf, Long idPauta);

}
