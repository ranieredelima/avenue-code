package br.com.ranieredelima.voting.annotations.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import br.com.ranieredelima.voting.annotations.aop.AbleToVoteValidation;

@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AbleToVoteValidation.class)
@Documented
public @interface AbleToVote {

	String message() default "";
	String field() default "";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
