package br.com.ranieredelima.voting.annotations.aop;

import java.lang.reflect.Field;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Component;

import br.com.ranieredelima.voting.annotations.annotation.AbleToVote;
import br.com.ranieredelima.voting.exception.InvalidRequestException;
import br.com.ranieredelima.voting.util.GeradorCpf;
import br.com.ranieredelima.voting.web.client.ValidationClientComponent;

//@Aspect
@Component
public class AbleToVoteValidation implements ConstraintValidator<AbleToVote, Object> {

	private final ValidationClientComponent externalValidation;
	private String field;

	public AbleToVoteValidation(ValidationClientComponent externalValidation) {
		this.externalValidation = externalValidation;
	}

    @Override
    public void initialize(AbleToVote constraint) {
        this.field = constraint.field();
    }

//    @Pointcut("@annotation(br.com.ranieredelima.voting.annotations.annotation.AbleToVote)")
//    public void ableToVote() {
//    }
//
//    @Before("ableToVote()")
//    public void valid(JoinPoint joinPoint) {
//    }

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		System.out.println(value);
		
		try {
			Field declaredField = context.getClass().getSuperclass().getDeclaredField(this.field);
			declaredField.setAccessible(true);
			String cpf = (String) declaredField.get(value);
			
			if(cpf == null || cpf.length() == 0) {
				throw new InvalidRequestException("CPF não informado");
			} else if(!GeradorCpf.isCPF(cpf)) {
				throw new InvalidRequestException("CPF informado não é valido");
			}
			
			return externalValidation.validateClient(cpf);
			
		} catch (Exception e) {
			
			if(e instanceof InvalidRequestException) {
				throw (InvalidRequestException) e;
			}
			
			return false;
		}
	}
	
}
