package br.com.ranieredelima.voting.util;

public class MensagemConstantes {

	private MensagemConstantes() {
	}

	public static final String ACESSO_NEGADO = "ACESSO NEGADO, USUARIO NAO ESTA AUTENTICADO";
	public static final String USUARIO_SENHA_INVALIDO = "E-mail e/ou senha estão incorretos";
	public static final String ERRO_INTERNO = "Ocorreu um erro interno na aplicação. Por favor, entre em contato com o Administrador";
	public static final String NAO_ENCONTRADO_ERROR = "Informação não localizada";
	
}
