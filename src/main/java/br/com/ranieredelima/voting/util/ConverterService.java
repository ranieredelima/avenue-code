package br.com.ranieredelima.voting.util;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class ConverterService {

    private final ModelMapper modelMapper;

    public ConverterService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public <T> T convert(Object data, Class<T> type) {
        return modelMapper.map(data, type);
    }

    public <T> Page<T> convert(Page<?> dataList, Class<T> type) {
        return dataList.map(d -> convert(d, type));
    }

    public <T> List<T> convert(List<?> dataList, Class<T> type) {
        return dataList.stream().map(d -> convert(d, type)).collect(Collectors.toList());
    }

    public <T> T merge(Object source, T target) {
        modelMapper.map(source, target);
        return target;
    }
}
