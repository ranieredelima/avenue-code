package br.com.ranieredelima.voting.service;

import org.springframework.data.domain.Pageable;

import br.com.ranieredelima.voting.web.v1.response.ViewResponse;

public interface ViewService {

	ViewResponse listarVotacoes(Pageable pageable);
	ViewResponse cadastrarVotacao();
	ViewResponse realizarVotacao(Long idVotacao);
}
