package br.com.ranieredelima.voting.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.ranieredelima.voting.exception.NotFoundException;
import br.com.ranieredelima.voting.model.api.Pauta;
import br.com.ranieredelima.voting.repository.PautaRepository;
import br.com.ranieredelima.voting.service.ViewService;
import br.com.ranieredelima.voting.util.MensagemConstantes;
import br.com.ranieredelima.voting.web.v1.assembler.ViewAssembler;
import br.com.ranieredelima.voting.web.v1.response.ViewResponse;

@Service
public class ViewServiceImpl implements ViewService {

	private final PautaRepository pautaRepository;
	private ViewAssembler viewAssembler;
	

	public ViewServiceImpl(
		PautaRepository pautaRepository,
		ViewAssembler viewAssembler
	) {
		this.pautaRepository = pautaRepository;
		this.viewAssembler = viewAssembler;
	}


	@Override
	public ViewResponse listarVotacoes(Pageable pageable) {
		
		Page<Pauta> pagePauta = this.pautaRepository.findAll(pageable);
		return this.viewAssembler.assembler(pagePauta.getContent());
	}

	@Override
	public ViewResponse cadastrarVotacao() {
		return this.viewAssembler.assemblerDefault();
	}
	
	@Override
	public ViewResponse realizarVotacao(Long idVotacao) {
		Pauta pauta = this.pautaRepository.findById(idVotacao)
			.orElseThrow(() -> new NotFoundException(MensagemConstantes.NAO_ENCONTRADO_ERROR));

		return this.viewAssembler.assembler(pauta);
	}


}
