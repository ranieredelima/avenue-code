package br.com.ranieredelima.voting.service;

import br.com.ranieredelima.voting.web.v1.request.BodyRequest;

public interface PautaService {

	Object cadastrar(BodyRequest resquest);
	void inicarVotacao(BodyRequest resquest);
	void votar(BodyRequest request);
	Object obterDados(Long id);
	
}
