package br.com.ranieredelima.voting.service.impl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import br.com.ranieredelima.voting.exception.InvalidRequestException;
import br.com.ranieredelima.voting.exception.NotFoundException;
import br.com.ranieredelima.voting.model.api.Pauta;
import br.com.ranieredelima.voting.model.api.PautaStatus;
import br.com.ranieredelima.voting.model.api.TipoVoto;
import br.com.ranieredelima.voting.model.api.Voto;
import br.com.ranieredelima.voting.repository.PautaRepository;
import br.com.ranieredelima.voting.repository.VotoRepository;
import br.com.ranieredelima.voting.service.PautaService;
import br.com.ranieredelima.voting.util.ConverterService;
import br.com.ranieredelima.voting.web.client.ValidationClientComponent;
import br.com.ranieredelima.voting.web.v1.request.BodyRequest;
import br.com.ranieredelima.voting.web.v1.request.PautaRequest;
import br.com.ranieredelima.voting.web.v1.request.VotoRequest;

@Service
public class PautaServiceImpl implements PautaService {

	private static final int TEMPO_PADRAO = 1;
	
	private ValidationClientComponent validationClient;
	private ConverterService converterService;
	private PautaRepository pautaRepository;
	private VotoRepository votoRepository;
    private final TaskScheduler taskScheduler;

	public PautaServiceImpl(
		ValidationClientComponent validationClient,
		ConverterService converterService,
		PautaRepository pautaRepository,
		VotoRepository votoRepository,
	    TaskScheduler taskScheduler
	) {
		this.validationClient = validationClient;
		this.converterService = converterService;
		this.pautaRepository = pautaRepository;
		this.votoRepository = votoRepository;
		this.taskScheduler = taskScheduler;
	}
	
	@Override
	public Object cadastrar(BodyRequest request) {
		PautaRequest pautaRequest = this.converterService.convert(request.getCampos(), PautaRequest.class);
		
		Pauta pauta = this.converterService.convert(pautaRequest, Pauta.class);
		
		pauta.setMinutos( pauta.getMinutos() == 0 ? TEMPO_PADRAO : pauta.getMinutos() );
		pauta.setStatus(PautaStatus.CRIADA);
		pauta.setCriadoPor(request.getCpf());
		
		this.pautaRepository.save(pauta);
		
		return pautaRequest;
	}

	@Override
	public void inicarVotacao(BodyRequest request) {
		Pauta pauta = this.pautaRepository.findById(request.getIdentificador())
							.orElseThrow(
								() -> new NotFoundException("NÃO EXISTE PAUTA COM ESSE IDENTIFICADOR")
							);
		
		pauta.setStatus(PautaStatus.EM_VOTACAO);
		this.pautaRepository.save(pauta);
		
        Instant time = LocalDateTime.now()
	        .atZone(ZoneId.systemDefault())
	        .plus(pauta.getMinutos(), ChronoUnit.MINUTES)
	        .toInstant();

        taskScheduler.schedule(
	    	() -> this.finalizarVotacao(pauta.getId()),
	    	time
	    );
		
	}
	
	private void finalizarVotacao (Long idPauta) {
		Pauta pauta = this.pautaRepository.findById(idPauta)
							.orElseThrow(
								() -> new NotFoundException("NÃO EXISTE PAUTA COM ESSE IDENTIFICADOR")
							);
		
		pauta.setStatus(PautaStatus.FINALIZADA);
		this.pautaRepository.save(pauta);
	}

	@Override
	public void votar(BodyRequest request) {
		this.validationClient.validateClient(request.getCpf());
		
		Pauta pauta = this.pautaRepository.findById(request.getIdentificador())
			.orElseThrow(
				() -> new NotFoundException("NÃO EXISTE PAUTA COM ESSE IDENTIFICADOR")
			);
		
		if(!PautaStatus.EM_VOTACAO.equals(pauta.getStatus())) {
			throw new InvalidRequestException("NÃO É POSSIVEL VOTAR EM UMA PAUTA NÃO INICIADA");
		}
		
		if(this.votoRepository.existsByCpfAndIdPauta(request.getCpf(), request.getIdentificador())) {
			throw new InvalidRequestException("CPF JÁ VOTOU PARA ESSA PAUTA");
		}
		
		VotoRequest votoRequest = this.converterService.convert(request.getCampos(), VotoRequest.class);
		
		Voto voto = this.converterService.convert(votoRequest, Voto.class);
		
		if(voto.getVoto() == null) {
			voto.setVoto(TipoVoto.valueOf(request.getDadosOpcao()));
		}
		
		voto.setCpf(request.getCpf());
		voto.setIdPauta(request.getIdentificador());
		
		this.votoRepository.save(voto);

	}

	@Override
	public Pauta obterDados(Long id) {
		return this.pautaRepository.findById(id)
				.orElseThrow(
						() -> new NotFoundException("NÃO EXISTE PAUTA COM ESSE IDENTIFICADOR")
					);
	}

}
