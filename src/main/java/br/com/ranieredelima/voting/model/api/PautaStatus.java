package br.com.ranieredelima.voting.model.api;

public enum PautaStatus {

	CRIADA,
	EM_VOTACAO,
	FINALIZADA
	
}
