package br.com.ranieredelima.voting.model.api;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "T_PAUTAS", schema = "VOTING")
@NoArgsConstructor
@AllArgsConstructor
@Data
@GenericGenerator(
        name = "SEQ_PAUTA",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(name = "sequence_name", value = "VOTING.S_PAUTA"),
                @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
        }
)
public class Pauta {
	
	@Id
    @GeneratedValue(generator = "SEQ_PAUTA")
    private Long id;
	
	private String titulo;
	
	private int minutos;
	
	private LocalDate dataVotacao;
	
	private String criadoPor;
	
	@Enumerated(EnumType.STRING)
	private PautaStatus status;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@Formula("(select count(1) from VOTING.t_votos v where v.id_pauta = id and v.voto = 'NAO')")
	private long votosNao;

	@Formula("(select count(1) from VOTING.t_votos v where v.id_pauta = id and v.voto = 'SIM')")
	private long votosSim;
	

}
