package br.com.ranieredelima.voting.model.api;

public enum TipoVoto {

	SIM,
	NAO,
	TALVEZ
}
