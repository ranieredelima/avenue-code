package br.com.ranieredelima.voting.model.api;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "T_VOTOS", schema = "VOTING")
@NoArgsConstructor
@AllArgsConstructor
@Data
@GenericGenerator(
        name = "SEQ_PAUTA",
        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(name = "sequence_name", value = "VOTING.S_PAUTA"),
                @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
        }
)
public class Voto {
	
	@Id
    @GeneratedValue(generator = "SEQ_PAUTA")
    private Long id;
	
    private Long idPauta;
    private String cpf;

    @Enumerated(EnumType.STRING)
    private TipoVoto voto;

}
